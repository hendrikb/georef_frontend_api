# API Endpoint & Frontend for georefscraper

This is the frontend and API show reel, demo, tutorial on how to use persisted data from the georefscraper project, which can be found here:

https://gitlab.com/hendrikb/georefscraper

## Installation

Since this project depends on the georefscraper project mentioned above, we
assume you have a working system environment with ruby et al.

If not, set up your system like described here:
https://gitlab.com/hendrikb/georefscraper

Then continue with setting up this project.

1. Get this project's source code: ```wget https://gitlab.com/hendrikb/georef_frontend_api/repository/archive.zip?ref=master -O frontend.zip```
2. Unzip the archive  ```unzip frontend.zip```
3. Change to the frontend folder: ```cd georef_frontend_api-master-*```
4. Install all the dependencies: ```bundle install```
5. Edit the ```config/database.yml``` according to the prototyp backend. Mind the environments!
6. Run the server: ```ENV=production rackup -E production```

Now open your browser at http://localhost:9292 - If you have data in your
database, you should be able to see it here directly. Otherwise the web site
will be empty.

See ```rackup --help``` for options where to let the server listen, adjust the
port, etc.
