require 'yaml'
require 'sequel'
require 'geo'
require 'sinatra/base'
require 'sinatra/namespace'

ENV['ENV'] = 'development' unless ENV['ENV']

DB_CONFIG = YAML.load_file(File.join('config', 'database.yml'))[ENV['ENV']]
DB = Sequel.connect(DB_CONFIG)

require 'sinatra/base'

module GeorefscraperAPI
  class App < Sinatra::Base
    register Sinatra::Namespace

    get '/' do
      @social_networks = DB[:social_networks].map do |sn|
        {
          id: sn[:id], name: sn[:name], created_at: sn[:created_at],
          actors: actors_for(sn[:id])
        }
      end

      haml :index
    end

    def actors_for(social_network_id)
      DB[:actors].where(social_network_id: social_network_id).map do |a|
        {
          id: a[:id],
          label: a[:label],
          class: a[:class],
          links: relationships_for(a[:id]).reject { |rel_a| rel_a[:label] == a[:label] },
          relations: relations_for(a[:id])
        }
      end
    end

    def relationships_for(actor_id)
      DB["SELECT other.label FROM relationships AS rs, actors as other WHERE ((rs.source = #{actor_id} AND other.id = rs.target) OR (rs.target = #{actor_id} AND other.id = rs.source));"]
    end

    def relations_for(actor_id)
      DB["SELECT eml.value AS eml_value, eml.type as eml_type, r.type, emr.value AS emr_value, emr.type AS emr_type FROM entity_mentions AS emr, entity_mentions AS eml, relations AS r WHERE r.actor_id = #{actor_id} AND r.right = emr.id AND r.left = eml.id GROUP BY emr_value;"]
    end

    namespace '/georeference' do
      namespace '/v1' do
        nil_time = nil
        get '/:graph/:actor', provides: [:json] do
          social_network = DB[:social_networks].where(id: params[:graph]).first
          actor = DB[:actors].where(id: params['actor']).first

          results = []

          DB[:relations].where(actor_id: actor[:id]).each do |relation|
            search_result = DB[:search_results].where(id: relation[:search_result_id]).first

            DB[:entity_mentions].where(id: relation[:right]).each do |location|
              osm_location = Geo::OSM::Location.new(location[:value],
                                                    location[:value],
                                                    'unknown_not_geocoded')

              results << Geo::Reference.new(osm_location, [
                                              Geo::Discovery::Base.new(Geo::Discovery::Source.new(search_result[:search_provider_origin], search_result[:confidence], {}),
                                                                       nil_time,
                                                                       Geo::Discovery::Locator.new(search_result[:resource], -1, search_result[:created_at], '', {}), {}
                                                                      )]
                                           )
            end
          end

          JSON.pretty_generate(actor: actor[:label],
                               graph: social_network[:name],
                               results: results)
        end
      end
    end

    get '/ping' do
      'pong!'
    end

    # start the server if ruby file executed directly
    run! if app_file == $PROGRAM_NAME
  end
end
